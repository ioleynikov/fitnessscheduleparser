package ru.cottoffeus;

import ru.cottoffeus.beans.ICalendar.VCalendar;
import ru.cottoffeus.connectors.HttpConnector;
import ru.cottoffeus.parsers.ParserTerFitPage;
import ru.cottoffeus.senders.EmailSender;
import ru.cottoffeus.workers.WorkerICalWriter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");
        try {
            if (args.length > 0 && (args[0] != null) && !args[0].isEmpty()) {
                if(args[0].contains("terfit.ru/schedule")) {
                    HttpConnector pageConnector = new HttpConnector(args[0]);
                    String pageContent = pageConnector.getPageContent();
                    System.out.println("Start parsing www.terfit.ru/schedule.\n");
                    ParserTerFitPage terFitPageParser = new ParserTerFitPage();
                    VCalendar calendar = terFitPageParser.parse(pageContent);
                    WorkerICalWriter calWriter = new WorkerICalWriter();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Calendar currentDate = Calendar.getInstance();
                    System.getProperty("os.name");
                    File outputDir = new File (System.getProperty("user.dir") + System.getProperty("file.separator") + "out" + System.getProperty("file.separator") + "generatedCalendarFiles");
                    System.out.println("I'll try to make dir");
                    if(!outputDir.exists()){
                        try {
                            if (outputDir.mkdir()){
                                System.out.println("Directory created.");
                            }
                        } catch (SecurityException e) {
                            e.printStackTrace();
                            System.out.println("Unable to create directory.");
                        }

                    }

                    File calendarFile = new File ( outputDir.getAbsolutePath() + System.getProperty("file.separator")+
                            "terfitShedule" + sdf.format(currentDate.getTime()) + ".ics");
                    calWriter.writeCalendar(calendar, calendarFile);
                    EmailSender sender = new EmailSender();
                    sender.sendEmail(calendarFile);

                } else{
                    System.out.println("Parser for this site is note avaliable!\n");
                }
            } else {
                System.out.println("Arguments are empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
