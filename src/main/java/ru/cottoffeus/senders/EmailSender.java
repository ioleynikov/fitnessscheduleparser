package ru.cottoffeus.senders;

import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Java Program to send text mail using default SMTP server and without authentication.
 * You need mail.jar, smtp.jar and activation.jar to run this program.
 *
 * @author Javin Paul
 */

public class EmailSender {

    private String username = "scheduleparser";
    private String password = "2aPTyr#PkiA2";
    private Properties props;

    public void sendEmail(File calendarFile) {


        String to = "cottoffeus@gmail.com";         //  receiver email
        String from = "scheduleparser@mail.ru";       //  sender email

        props = new Properties();
        props.put("mail.smtp.host", "smtp.mail.ru");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session); // email message
            message.setFrom(new InternetAddress(from)); // setting header fields
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("���������� ������� �� ������� ������"); // subject line
            message.setText("�� �������� ���������� ������� �� ������� ������");

            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            DataSource fileDataSource = new FileDataSource(calendarFile);
            messageBodyPart.setDataHandler(new DataHandler(fileDataSource));
            messageBodyPart.setText("�� �������� ���������� ������� �� ������� ������");
            messageBodyPart.setFileName(calendarFile.getName());
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            // Send message
            Transport.send(message);
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}