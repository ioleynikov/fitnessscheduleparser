package ru.cottoffeus.beans.ICalendar;

import java.util.ArrayList;

public class VEvent {
    String dtstart = "";
    String dtend = "";
    String dtstamp = "";
    String uid = "";
    String created = "";
    String description = "";
    String lastModified = "";
    String location = "";
    String sequence = "";
    String status = "";
    String summary = "";
    String transp = "";
    ArrayList<VAlarm> vAlarms = new ArrayList<>();

    public String getDtstart() {
        return dtstart;
    }

    public void setDtstart(String dtstart) {
        this.dtstart = dtstart;
    }

    public String getDtend() {
        return dtend;
    }

    public void setDtend(String dtend) {
        this.dtend = dtend;
    }

    public String getDtstamp() {
        return dtstamp;
    }

    public void setDtstamp(String dtstamp) {
        this.dtstamp = dtstamp;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTransp() {
        return transp;
    }

    public void setTransp(String transp) {
        this.transp = transp;
    }

    public ArrayList<VAlarm> getvAlarms() {
        return vAlarms;
    }

    public void addvAlarms(VAlarm vAlarm) {
        this.vAlarms.add(vAlarm);
    }

    public String getValuesForHash() {
        return (getSummary().concat(getDtstart()).concat(getDtend()).concat(getDescription())).replaceAll("[ ,.\n�:-]", "").toUpperCase();
    }

    public String toString() {
        return "������: " + getDtstart() +
                "\n���������: " + getDtend() +
                "\n���������: " + getSummary() +
                "\n" + getDescription() +
                "\n�����: " + getLocation() + "\n";
    }

}
