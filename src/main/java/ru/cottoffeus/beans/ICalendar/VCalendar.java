package ru.cottoffeus.beans.ICalendar;

import java.util.ArrayList;

public class VCalendar {
    String version;
    String prodid;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getProdid() {
        return prodid;
    }

    public void setProdid(String prodid) {
        this.prodid = prodid;
    }

    ArrayList<VEvent> VEvents = new ArrayList<>();

    public ArrayList<VEvent> getVEvents() {
        return this.VEvents;
    }

    public void addVEvent(VEvent vevent) {
        this.VEvents.add(vevent);
    }

    public void addVEvents(ArrayList<VEvent> vevents) {
        this.VEvents.addAll(vevents);
    }

    public VCalendar(String version, String prodid) {
        this.version = version;
        this.prodid = prodid;
    }

}
