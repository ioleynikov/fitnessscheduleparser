package ru.cottoffeus.connectors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpConnector {
    private HttpURLConnection connection;

    public HttpConnector(String stringPageUrl) {
        try {
            this.connection = createConnection(new URL(stringPageUrl));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Connection Failed!\n");
            this.connection = null;
        }
    }

    private HttpURLConnection createConnection(URL pageURL) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) pageURL.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(1000);
        connection.setReadTimeout(6000);
        return connection;
    }

    public String getPageContent() {
        try {
            if (connection != null) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                return content.toString();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
