package ru.cottoffeus.parsers;

import ru.cottoffeus.beans.ICalendar.VCalendar;
import ru.cottoffeus.beans.ICalendar.VEvent;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserTerFitPage {
    private static DocumentBuilder builder = null;
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static int counter = 0;
    private static int substringEnd;
    private static Pattern DIV = Pattern.compile("(<div)|(/>)|(</div>)");

    public static void setSubstringEnd(int substringEnd) {
        if (ParserTerFitPage.substringEnd < substringEnd) {
            ParserTerFitPage.substringEnd = substringEnd;
        }
    }

    private DocumentBuilder getBuilder() throws ParserConfigurationException {
        if (builder == null) {
            builder = factory.newDocumentBuilder();
        }
        return builder;
    }


    public VCalendar parse(String pageContent) throws IOException, SAXException, ParserConfigurationException {
        return parseShedule(extractShedule(pageContent));
    }


    private String extractShedule(String pageContent) {
        String shedule = "";
        int sheduleSubstringStart = 0;
        int sheduleSubstringEnd = 0;
        Pattern startPattern = Pattern.compile("<div class=\"scp-days__wrapper\" id=\"scpDaysWrapper\">");
        Matcher startMatcher = startPattern.matcher(pageContent);
        if (startMatcher.find()) {
            sheduleSubstringStart = startMatcher.start();
            if (sheduleSubstringStart > -1) counter++;
            sheduleSubstringEnd = getSheduleSubstringEnd(startMatcher.end(), pageContent);
        }
        return pageContent.substring(sheduleSubstringStart, sheduleSubstringEnd).replaceAll("\t", "").trim();
    }

    private int getSheduleSubstringEnd(int searchStart, String pageContent) {
        Matcher divMatch = DIV.matcher(pageContent);
        divMatch.region(searchStart, pageContent.length() - 1);
        if (divMatch.find()) {
            String currentGroup = divMatch.group();
            if (counter > 0) {
                setSubstringEnd(divMatch.end());
                if (currentGroup.equals("<div")) {
                    counter++;
                    getSheduleSubstringEnd(divMatch.end(), pageContent);
                } else if (currentGroup.equals("/>") || currentGroup.equals("</div>")) {
                    counter--;
                    getSheduleSubstringEnd(divMatch.end(), pageContent);
                }
            }
            return substringEnd;
        }
        return substringEnd;
    }

    private VCalendar parseShedule(String shedule) throws ParserConfigurationException, IOException, SAXException {
        VCalendar calendar = new VCalendar("2.0", "FitnessSheduleParser");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Document sheduleDom = getBuilder().parse(new ByteArrayInputStream(shedule.getBytes(StandardCharsets.UTF_8)));
        Element root = sheduleDom.getDocumentElement();
        ArrayList<Node> dates = getElementsByStartClassName(root, "scp-day__title");
        ArrayList<Node> days = getElementsByStartClassName(root, "scp-day js-day");
        Iterator<Node> datesIt = dates.iterator();
        Iterator<Node> daysIt = days.iterator();


        while (datesIt.hasNext() && daysIt.hasNext()) {
            Node date = datesIt.next();
            Node day = daysIt.next();
            ArrayList<Node> classes = getElementsByStartClassName(day, "scp-tile");
            for (Node class_ : classes) {
                VEvent vEvent = new VEvent();

                String description = "";
                ArrayList<Node> temp = getElementsByStartClassName(class_, "scp-tile__trainer");
                if (!temp.isEmpty()) {
                    description = description.concat("������ - " + temp.get(0).getTextContent());
                }
                temp = getElementsByStartClassName(class_, "scp-tile__place");
                if (!temp.isEmpty()) {
                    vEvent.setLocation(temp.get(0).getTextContent());
                }
                temp = getElementsByStartClassName(class_, "scp-tile__title");
                if (!temp.isEmpty()) {
                    vEvent.setSummary(  temp.get(0).getTextContent());
                }
                vEvent.setDescription(description);
                Calendar dateToFormat = Calendar.getInstance();
                String[] dateMounth = date.getFirstChild().getNextSibling().getTextContent().split("\\.");
                dateToFormat.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateMounth[0]));
                dateToFormat.set(Calendar.MONTH, Integer.parseInt(dateMounth[1]) - 1);

                String time = getElementsByStartClassName(getElementsByStartClassName(class_, "scp-tile__header").get(0), "scp-tile__time").get(0).getTextContent();
                String startTime = time.split(" � ")[0];
                String endTime = time.split(" � ")[1];
                vEvent.setDtstart(sdf.format(dateToFormat.getTime()) + "T" + startTime.split(":")[0] + startTime.split(":")[1] + "00");
                vEvent.setDtend(sdf.format(dateToFormat.getTime()) + "T" + endTime.split(":")[0] + endTime.split(":")[1] + "00");

                vEvent.setUid(String.valueOf(vEvent.getValuesForHash().hashCode()));
                //Put logic here

//                    VAlarm alarm = new VAlarm();
//                    alarm.setTrigger(VConsts.TRIG_DAY_BEFORE);
//                    alarm.setDescription("����������� � ����������");
//                    alarm.setAction(VConsts.ACT_DISPLAY);
//                    vEvent.addvAlarms(alarm);
//
//                    alarm.setTrigger(VConsts.TRIG_30M_BEFORE);
//                    alarm.setDescription("�����������");
//                    alarm.setAction(VConsts.ACT_DISPLAY);
//                    vEvent.addvAlarms(alarm);

                System.out.println(vEvent.getUid());
                System.out.println(vEvent);
                calendar.addVEvent(vEvent);
            }
        }


        return calendar;
    }

    private ArrayList<Node> getElementsByStartClassName(Element element, String className) {
        NodeList nodeList = element.getElementsByTagName("div");
        return getNodes(className, nodeList);
    }

    private ArrayList<Node> getElementsByStartClassName(Node node, String className) {
        NodeList nodeList = node.getChildNodes();
        return getNodes(className, nodeList);
    }

    private ArrayList<Node> getNodes(String className, NodeList nodeList) {
        ArrayList<Node> resultNodeList = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            NamedNodeMap attributes = nodeList.item(i).getAttributes();
            if (attributes!= null && attributes.getNamedItem("class").getNodeValue().startsWith(className)) {
                resultNodeList.add(nodeList.item(i));
            }
        }
        return resultNodeList;
    }

}
