package ru.cottoffeus.workers;

import ru.cottoffeus.beans.ICalendar.VAlarm;
import ru.cottoffeus.beans.ICalendar.VCalendar;
import ru.cottoffeus.beans.ICalendar.VEvent;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class WorkerICalWriter implements Worker {

    private Writer fileWriter;

    @Override
    public void writeFile(VCalendar calendar, File file) {
        this.writeCalendar(calendar, file);
    }

    public void writeCalendar(VCalendar calendar, File file) {
        try {
            this.fileWriter = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
            fileWriter.append("BEGIN:VCALENDAR\n");

            fileWriter.append("PRODID:").append(calendar.getProdid()).append("\n");
            fileWriter.append("VERSION:").append(calendar.getVersion()).append("\n");
            fileWriter.append("METHOD:PUBLISH\n");
            this.writeEvents(calendar.getVEvents());
            fileWriter.append("END:VCALENDAR");
            fileWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void writeEvents(ArrayList<VEvent> events) throws IOException {
        for(VEvent event : events) {
            fileWriter.append("BEGIN:VEVENT\n");
            fileWriter.append("DTSTART:").append(event.getDtstart()).append("\n");
            fileWriter.append("DTEND:").append(event.getDtend()).append("\n");
            fileWriter.append("UID:").append(event.getUid()).append("\n");
            fileWriter.append("DESCRIPTION:").append(event.getDescription()).append("\n");
            fileWriter.append("LOCATION:").append(event.getLocation()).append("\n");
            fileWriter.append("SUMMARY:").append(event.getSummary()).append("\n");
            this.writeAlarms(event.getvAlarms());
            fileWriter.append("END:VEVENT").append("\n");


        }
    }

    private void writeAlarms(ArrayList<VAlarm> alarms) throws IOException {
        for (VAlarm alarm : alarms){
            fileWriter.append("BEGIN:VALARM\n");
            fileWriter.append("ACTION:").append(alarm.getAction()).append("\n");
            fileWriter.append("DESCRIPTION:").append(alarm.getDescription()).append("\n");
            fileWriter.append("TRIGGER:").append(alarm.getTrigger()).append("\n");
            fileWriter.append("END:VALARM\n");
        }
    }


}
