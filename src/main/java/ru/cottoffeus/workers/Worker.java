package ru.cottoffeus.workers;

import ru.cottoffeus.beans.ICalendar.VCalendar;

import java.io.File;

public interface Worker {
    public void writeFile(VCalendar calendar, File file);
}
